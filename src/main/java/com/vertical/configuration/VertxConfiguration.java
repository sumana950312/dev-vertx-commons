package com.vertical.configuration;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hazelcast.core.HazelcastInstance;

import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.ServiceDiscoveryOptions;
import io.vertx.serviceproxy.ServiceBinder;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;

@Configuration
public class VertxConfiguration {
	private static final Logger log = LoggerFactory.getLogger(VertxConfiguration.class);

	@Autowired
	private HazelcastInstance hazelcastInstance;

	@Value("${vertx.worker.pool.size}")
	private int workerPoolSize;

	private Vertx vertx;
	private ServiceDiscovery serviceDiscovery;
	private ServiceBinder serviceBinder;
	
	@Bean(destroyMethod = "")
	public Vertx vertx() {
		return vertx;
	}

	@Bean
	public ServiceDiscovery serviceDiscovery() {
		return serviceDiscovery;
	}
	
	@Bean
	public ServiceBinder serviceBinder() {
		return serviceBinder;
	}
	
	@PostConstruct
	public void init() throws UnknownHostException, InterruptedException, ExecutionException {
		VertxOptions options = new VertxOptions();
		options.setMaxEventLoopExecuteTime(Long.MAX_VALUE);
		options.setClustered(true);
		options.setClusterManager(new HazelcastClusterManager(hazelcastInstance));
		options.setClusterHost(Inet4Address.getLocalHost().getHostAddress());
		options.setWorkerPoolSize(workerPoolSize);

		ServiceDiscoveryOptions sdOptions = new ServiceDiscoveryOptions().setAnnounceAddress("service-announce")
				.setName("vertical");

		CompletableFuture<Vertx> future = new CompletableFuture<Vertx>();
		Vertx.clusteredVertx(options, ar -> {
			if (ar.succeeded()) {
				log.debug("Vertx instance declared succesful");
				future.complete(ar.result());
			} else {
				future.completeExceptionally(ar.cause());
			}
		});

		vertx = future.get();
		serviceDiscovery = ServiceDiscovery.create(vertx, sdOptions);
		serviceBinder = new ServiceBinder(vertx);
	}

	@PreDestroy
	public void close() throws ExecutionException, InterruptedException {
		CompletableFuture<Void> future = new CompletableFuture<>();
		vertx.close(ar -> future.complete(null));
		future.get();
	}
}
