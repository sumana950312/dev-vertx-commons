package com.vertical.verticles;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.impl.ConcurrentHashSet;
import io.vertx.core.json.JsonObject;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.types.EventBusService;
import io.vertx.servicediscovery.types.HttpEndpoint;
import io.vertx.servicediscovery.types.MessageSource;
import io.vertx.serviceproxy.ServiceBinder;

@Component
@Scope(SCOPE_PROTOTYPE)
@ComponentScan("com.vertical")
public class MicroServiceVerticle extends AbstractVerticle {

	@Autowired
	protected ServiceDiscovery discovery;
	
	@Autowired
	protected ServiceBinder serviceBinder;

	protected Set<Record> registeredRecords = new ConcurrentHashSet<>();

	public void publishHttpEndpoint(String name, String host, int port, Handler<AsyncResult<Void>> completionHandler) {
		Record record = HttpEndpoint.createRecord(name, host, port, "/");
		publish(record, completionHandler);
	}

	public void publishMessageSource(String name, String address, String host, Class<?> contentClass, Handler<AsyncResult<Void>> completionHandler) {
		Record record = MessageSource.createRecord(name, address, contentClass);
		publish(record, completionHandler);
	}

	public void publishMessageSource(String name, String address, String host, Handler<AsyncResult<Void>> completionHandler) {
		Record record = MessageSource.createRecord(name, address);
		publish(record, completionHandler);
	}

	public void publishEventBusService(String name, String address, Class<?> serviceClass, boolean authentication, boolean exposed, Handler<AsyncResult<Void>> completionHandler) {
		JsonObject metadata = new JsonObject().put("authentication", authentication).put("exposed", exposed); 
		Record record = EventBusService.createRecord(name, address, serviceClass, metadata);
		publish(record, completionHandler);
	}
	
	public <T> void bindService(String Address, Class<T> clazz, T instance, Handler<AsyncResult<Void>> resultHandler) {
		serviceBinder.setAddress(Address).register(clazz, instance).completionHandler(resultHandler);
	}
	
	protected void publish(Record record, Handler<AsyncResult<Void>> completionHandler) {
		discovery.publish(record, ar -> {
			if (ar.succeeded()) {
				registeredRecords.add(record);
			}
			completionHandler.handle(ar.map((Void) null));
		});
	}

	@Override
	public void stop(Future<Void> stopFuture) throws Exception {
		List<Future> futures = new ArrayList<>();
		for (Record record : registeredRecords) {
			Future<Void> unregistrationFuture = Future.future();
			futures.add(unregistrationFuture);
			discovery.unpublish(record.getRegistration(), unregistrationFuture.completer());
		}
		
		if(futures.isEmpty()) {
			discovery.close();
			stopFuture.complete();
		} else {
			CompositeFuture compositeFuture = CompositeFuture.all(futures);
			compositeFuture.setHandler(ar -> {
				discovery.close();
				if(ar.failed()) {
					stopFuture.fail(ar.cause());
				} else {
					stopFuture.complete();
				}
			});
		}
	}
	
}
