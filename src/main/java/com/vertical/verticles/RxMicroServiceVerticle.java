package com.vertical.verticles;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.rx.java.ObservableFuture;
import io.vertx.rx.java.RxHelper;
import io.vertx.rxjava.servicediscovery.ServiceDiscovery;
import io.vertx.rxjava.servicediscovery.types.EventBusService;
import io.vertx.rxjava.servicediscovery.types.HttpEndpoint;
import io.vertx.rxjava.servicediscovery.types.MessageSource;
import io.vertx.servicediscovery.Record;
import rx.Single;

@Component
@Scope(SCOPE_PROTOTYPE)
@ComponentScan("com.vertical")
public class RxMicroServiceVerticle extends MicroServiceVerticle {
	@Autowired
	protected Vertx vertx;
	protected ServiceDiscovery rxDiscovery;

	@Override
	public void start() throws Exception {
		super.start();
		rxDiscovery = ServiceDiscovery.newInstance(super.discovery);
	}

	public Single<Void> rxPublishHttpEndpoint(String name, String host, int port) {
		Record record = HttpEndpoint.createRecord(name, host, port, "/");
		return rxPublish(record);
	}

	public Single<Void> rxPublishMessageSource(String name, String address) {
		Record record = MessageSource.createRecord(name, address);
		return rxPublish(record);
	}
	
	public <T> Single<Void> rxPublishEbService(String name, String address, Class<T> clazz) {
		JsonObject metadata = new JsonObject();
		Record record = EventBusService.createRecord(name, address, clazz.getSimpleName(), metadata);
		return rxPublish(record);
	}

	private Single<Void> rxPublish(Record record) {
		ObservableFuture<Void> adapter = RxHelper.observableFuture();
		publish(record, adapter.toHandler());
		return adapter.toSingle();
	}
}
