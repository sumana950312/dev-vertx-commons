package com.vertical.verticles.eventbus.proxy;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.vertical.verticles.eventbus.proxy.constants.ExecutionContants.*;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.serviceproxy.ServiceException;
import io.vertx.serviceproxy.ServiceExceptionMessageCodec;

@Component
public class GenericVertxEBProxy implements GenericEBProxyService {

	private final static Logger logger = LoggerFactory.getLogger(GenericVertxEBProxy.class);

	@Autowired
	private Vertx _vertx;
	private DeliveryOptions _options;
	private boolean closed;

	@PostConstruct
	public void onInit() {
		_options = null;
		try {
			this._vertx.eventBus().registerDefaultCodec(ServiceException.class, new ServiceExceptionMessageCodec());
		} catch (Exception e) {
			logger.error("Something goes wrong registering the codec: ", e);
		}
	}

	@Override
	public <T> void executeAsyncRpc(String address, String action, JsonObject payload,
			Handler<AsyncResult<T>> resultHandler) {
		if (closed) {
			resultHandler.handle(Future.failedFuture(new IllegalStateException("Proxy is closed")));
			return;
		}

		if (address.isEmpty() || address == null) {
			resultHandler.handle(Future.failedFuture("The service address must be not null"));
			return;
		}

		if (action == null) {
			action = DEFAULT_EXECUTION;
		}

		JsonObject _json = new JsonObject();
		if (payload != null) {
			_json.put("payload", payload);
		}

		DeliveryOptions _deliveryOptions = (_options != null) ? new DeliveryOptions(_options) : new DeliveryOptions();

		_deliveryOptions.addHeader("action", action);

		_vertx.eventBus().<JsonObject>send(address, _json, _deliveryOptions, res -> {
			if (res.failed()) {
				resultHandler.handle(Future.failedFuture(res.cause()));
			} else {
				resultHandler.handle(Future.succeededFuture((T) res.result().body()));
			}
		});
	}
}
