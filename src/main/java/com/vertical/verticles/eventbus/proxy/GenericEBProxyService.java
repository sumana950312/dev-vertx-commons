package com.vertical.verticles.eventbus.proxy;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

public interface GenericEBProxyService {
	public <T> void executeAsyncRpc(String address, String action, JsonObject payload,
			Handler<AsyncResult<T>> resultHandler);

}
