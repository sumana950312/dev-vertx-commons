package com.vertical.verticles.factories;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import io.vertx.core.Verticle;
import io.vertx.core.spi.VerticleFactory;

@Component
@ComponentScan("com.vertical")
public class SpringVerticleFactory implements VerticleFactory, ApplicationContextAware {

	@Value("${app.prefix}")
	protected String prefix;

	ApplicationContext applicationContext;

	@Override
	public boolean blockingCreate() {
		// the verticles instantiation in this case are spring beans, they might depend
		// on other beans or resources which needs to build or lookup, so in that way
		// this operation needs to be blocking
		return true;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	@Override
	public Verticle createVerticle(String verticleName, ClassLoader classLoader) throws Exception {
		// the given classname gonna be the verticle name
		String clazz = VerticleFactory.removePrefix(verticleName);
		return (Verticle) applicationContext.getBean(Class.forName(clazz));
	}

	@Override
	public String prefix() {
		// Application prefix, just an string that represents the prefix for the
		// application
		return prefix;
	}

}
