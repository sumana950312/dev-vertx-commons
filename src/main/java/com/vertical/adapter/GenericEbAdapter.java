package com.vertical.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vertical.verticles.eventbus.proxy.GenericVertxEBProxy;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.serviceproxy.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * The Class GenericEbAdapter.
 * @author sumana
 */
@Component
public class GenericEbAdapter {
	
	/** The generic vertx EB proxy. */
	@Autowired
	private GenericVertxEBProxy genericVertxEBProxy;
	
	/** The discovery. */
	@Autowired
	private ServiceDiscovery discovery;
	
	/**
	 * Discover address and send.
	 *
	 * @param serviceAddress the service address
	 * @param action the action
	 * @param payload the payload
	 * @param resultHandler the result handler
	 */
	public void discoverAddressAndSend(String serviceAddress, String action, JsonObject payload, Handler<AsyncResult<JsonObject>> resultHandler) {
		discovery.getRecord(r -> r.getLocation().getString("endpoint").equals(serviceAddress), ar -> {
			if(ar.succeeded()) {
				if(ar.result() != null) {
					genericVertxEBProxy.executeAsyncRpc(serviceAddress, action, payload, resultHandler);
				} else {
					resultHandler.handle(Future.failedFuture(new ServiceException(404, "No service candidate aviable")));
				}
			} else {
				resultHandler.handle(Future.failedFuture(ar.cause()));
			}
		});
	}
	
	/**
	 * Discover and send.
	 *
	 * @param serviceName the service name
	 * @param action the action
	 * @param payload the payload
	 * @param resultHandler the result handler
	 */
	public <T> void discoverAndSend(String serviceName, String action, JsonObject payload, Handler<AsyncResult<T>> resultHandler) {
		discovery.getRecord(r -> r.getName().equals(serviceName), ar -> {
			if(ar.succeeded()) {
				if(ar.result() != null) {
					genericVertxEBProxy.executeAsyncRpc(ar.result().getLocation().getString("endpoint"), action, payload, resultHandler);
				} else {
					resultHandler.handle(Future.failedFuture(new ServiceException(404, "No service candidate aviable")));
				}
			} else {
				resultHandler.handle(Future.failedFuture(ar.cause()));
			}
		});
	}
}
